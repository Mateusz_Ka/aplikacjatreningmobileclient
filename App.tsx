import WelcomeScreen from './auth/screens/WelcomeScreen';
import { createSwitchNavigator, createAppContainer, NavigationContainer, NavigationScreenProp } from 'react-navigation';
import LoginScreen from './auth/screens/LoginScreen';
import RegisterScreen from './auth/screens/RegisterScreen';
import PlansListScreen from './main/screens/PlansListScreen';
import StartingScreen from './auth/screens/StartingScreen';
import { createStackNavigator } from 'react-navigation';
import PlanScreen from './main/screens/PlanScreen';
import TreningScreen from './main/screens/TreningScreen';
import DoneTreningScreen from './main/screens/DoneTreningScreen';

export default createAppContainer(createSwitchNavigator(
  {
    Start: StartingScreen,
    Home: WelcomeScreen,
    Login: LoginScreen,
    Register: RegisterScreen,
    App: createStackNavigator({ 
      PlanList: {
        screen: PlansListScreen,
        navigationOptions: ({ navigation }: any) => ({
          title: `Lista planów treningowych`,
        }),
      },
      Plan: {
        screen: PlanScreen,
        navigationOptions: ({ navigation }: any) => ({
          title: `${navigation.state.params.name}`,
        }),
        
      },
      Trening: {
        screen: TreningScreen,
        navigationOptions: ({ navigation }: any) => ({
          title: `${navigation.state.params.name}`,
        }),
      },
      DoneTrening: {
        screen: DoneTreningScreen,
        navigationOptions: ({ navigation }: any) => ({
          title: `${navigation.state.params.name}`,
        }),
      }
    })
  },
  {
    initialRouteName: 'Start',
  }
));