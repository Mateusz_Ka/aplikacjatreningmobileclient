import { AsyncStorage } from "react-native";

export async function isAuthenticated(): Promise<Boolean> {
    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) { return true; }
    return false;
}