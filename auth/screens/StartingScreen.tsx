import React, { Component } from 'react'
import { Text, View, StyleSheet, ActivityIndicator } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationProps } from '../types';

interface Props extends NavigationProps {

}

export default class StartingScreen extends Component<Props> {

    constructor(props: Props) {
        super(props);
        this._bootstrapAsync();
    }

    _bootstrapAsync = async () => {
        this.props.navigation.navigate('Home');
    };
    render() {
        return (
            <ActivityIndicator style={styles.container}></ActivityIndicator>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 30
    },
});
