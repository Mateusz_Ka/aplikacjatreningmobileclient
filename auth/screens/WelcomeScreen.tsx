import React, { Component } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import FatButton from '../components/FatButton';
import { NavigationProps } from '../types';

interface Props extends NavigationProps {

}

export default class WelcomeScreen extends Component<Props> {

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require("../../assets/png/logo.png")} />
                </View>
                <View style={styles.registerRow}>
                    <FatButton text="Logowanie" onPress={() => this.props.navigation.navigate('Login')}></FatButton>
                    <FatButton text="Rejestracja" onPress={() => this.props.navigation.navigate('Register')}></FatButton>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      padding: 30
    },
    registerRow: {
      flex: 2,
      alignItems: 'center',
      flexDirection: 'row',
    },
    googleRow: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'row',
    },
    logo: {
      flex: 5,
      alignItems: 'center',
      flexDirection: 'row',
    },
  });
