import React, { Component } from 'react'
import { View, StyleSheet, Keyboard } from 'react-native'
import { AuthInput, passwordInputProp, emailInputProp } from '../components/Input';
import validator from 'validator';
import FatButton from '../components/FatButton';
import { NavigationProps } from '../types';
import BackX from '../components/BackX';
import AsyncStorage from '@react-native-community/async-storage';

interface Props extends NavigationProps {

}

interface State {
    email: string;
    password: string;
    emailError?: string;
    passwordError?: string;
}

export default class LoginScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            email: "",
            password: "",
        }
    }

    private handleEmailInput = () => {
        if (validator.isEmpty(this.state.email)) {
            this.setState({ emailError: "email jest wymagany" })
        } else if (!validator.isEmail(this.state.email)) {
            this.setState({ emailError: "zły format emaila" })
        }
    }

    private handlePasswordInput = () => {
        if (validator.isEmpty(this.state.password)) {
            this.setState({ passwordError: "hasło jest wymagane" })
        } else if (!validator.isLength(this.state.password, { min: 6 })) {
            this.setState({ passwordError: "hasło jest za krótkie" })
        }
    }

    private isEmailValid(): boolean {
        return validator.isEmail(this.state.email)
    }

    private isPasswordValid(): boolean {
        return validator.isLength(this.state.password, { min: 6 })
    }

    private onPasswordChange = (password: string) => {
        this.setState({
            password,
            passwordError: this.isPasswordValid() ?
                undefined :
                this.state.passwordError
        });
    }

    private onEmailChange = (email: string) => {
        this.setState({
            email,
            emailError: this.isEmailValid() ?
                undefined :
                this.state.emailError
        });
    }

    private onSubmit = async () => {
        if (
            this.isEmailValid() &&
            this.isPasswordValid() &&
            this.state.password &&
            this.state.email
        ) {
            Keyboard.dismiss();
            await AsyncStorage.setItem("UserToken", "dhansj");
            this.props.navigation.navigate("App");
        } else {
            this.handleEmailInput();
            this.handlePasswordInput();
        }

    }

    public render() {
        return (
            <View style={styles.container}>
                <View style={styles.backButtonContainer}>
                <BackX navigation={this.props.navigation}></BackX>
                </View>
                <View style={styles.formContainer}>
                    <AuthInput
                        onEndEditing={this.handleEmailInput}
                        error={this.state.emailError}
                        inputType={emailInputProp}
                        value={this.state.email}
                        onChangeText={this.onEmailChange}
                    ></AuthInput>
                    <AuthInput
                        onEndEditing={this.handlePasswordInput}
                        inputType={passwordInputProp}
                        value={this.state.password}
                        error={this.state.passwordError}
                        onChangeText={this.onPasswordChange}
                    ></AuthInput>
                    <FatButton text="Zaloguj się" onPress={this.onSubmit}></FatButton>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    formContainer: {
        flex: 10,
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "center",
    },
    container: {
        flex:1,
        justifyContent: "center",
        padding: 20
    },
    backButtonContainer: {
        flex: 1,
        alignItems: "flex-end",
    }
})
