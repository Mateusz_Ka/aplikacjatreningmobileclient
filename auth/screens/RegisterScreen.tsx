import React, { Component } from 'react'
import { View, StyleSheet, Keyboard } from 'react-native'
import { NavigationProps } from '../types';
import validator from 'validator';
import BackX from '../components/BackX';
import { AuthInput, emailInputProp, passwordInputProp } from '../components/Input';
import FatButton from '../components/FatButton';
import AsyncStorage from '@react-native-community/async-storage';

interface Props extends NavigationProps {

}

interface State {
    email: string;
    password: string;
    repeatedPassword: string;
    emailError?: string;
    passwordError?: string;
    repeatedPasswordError?: string;
}

export default class RegisterScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            repeatedPassword: ""
        }
    }

    private handleEmailInput = () => {
        if (validator.isEmpty(this.state.email)) {
            this.setState({ emailError: "email jest wymagany" })
        } else if (!validator.isEmail(this.state.email)) {
            this.setState({ emailError: "zły format emaila" })
        }
    }

    private handlePasswordInput = () => {
        if (validator.isEmpty(this.state.password)) {
            this.setState({ passwordError: "hasło jest wymagane" })
        } else if (!validator.isLength(this.state.password, { min: 6 })) {
            this.setState({ passwordError: "hasło jest za krótkie" })
        }
    }

    private handleRepeatedPasswordInput = () => {
        if (validator.isEmpty(this.state.repeatedPassword)) {
            this.setState({ repeatedPasswordError: "hasło jest wymagane" })
        } else if (!validator.isLength(this.state.repeatedPassword, { min: 6 })) {
            this.setState({ repeatedPasswordError: "hasło jest za krótkie" })
        } else if (this.state.repeatedPassword !== this.state.password) {
            this.setState({ repeatedPasswordError: "hasła są różne" })
        }
    }

    private isEmailValid(): boolean {
        return validator.isEmail(this.state.email)
    }

    private isPasswordValid(): boolean {
        return validator.isLength(this.state.password, { min: 6 })
    }

    private isRepeatedPaswordValid(password: string = this.state.password): boolean {
        return validator.isLength(this.state.repeatedPassword, { min: 6 }) && password === this.state.repeatedPassword;

    }

    private onPasswordChange = (password: string) => {
        this.setState({
            password,
            passwordError: this.isPasswordValid() ?
                undefined :
                this.state.passwordError,
            repeatedPasswordError: this.isRepeatedPaswordValid(password) ?
                undefined :
                this.state.repeatedPasswordError
        });
    }

    private onRepeatedPasswordChange = (repeatedPassword: string) => {
        this.setState({
            repeatedPassword,
            repeatedPasswordError: this.isRepeatedPaswordValid() ?
                undefined :
                this.state.repeatedPasswordError
        });
    }

    private onEmailChange = (email: string) => {
        this.setState({
            email,
            emailError: this.isEmailValid() ?
                undefined :
                this.state.emailError
        });
    }

    private onSubmit = async () => {
        if (
            this.isPasswordValid() &&
            this.isEmailValid() &&
            this.isRepeatedPaswordValid()
        ) {
            Keyboard.dismiss();
            await AsyncStorage.setItem("UserToken", "dhansj");
            this.props.navigation.navigate("App");
        } else {
            this.handleEmailInput();
            this.handlePasswordInput();
            this.handleRepeatedPasswordInput();
        }

    }

    public render() {
        const repeatedPasswordProp = {
            ...passwordInputProp
        }
        repeatedPasswordProp.placeholder = "Powtóż hasło"
        return (
            <View style={styles.container}>
                <View style={styles.backButtonContainer}>
                    <BackX navigation={this.props.navigation}></BackX>
                </View>
                <View style={styles.formContainer}>
                    <AuthInput
                        onEndEditing={this.handleEmailInput}
                        error={this.state.emailError}
                        inputType={emailInputProp}
                        value={this.state.email}
                        onChangeText={this.onEmailChange}
                    ></AuthInput>
                    <AuthInput
                        onEndEditing={this.handlePasswordInput}
                        inputType={passwordInputProp}
                        value={this.state.password}
                        error={this.state.passwordError}
                        onChangeText={this.onPasswordChange}
                    ></AuthInput>
                    <AuthInput
                        onEndEditing={this.handleRepeatedPasswordInput}
                        inputType={repeatedPasswordProp}
                        value={this.state.repeatedPassword}
                        error={this.state.repeatedPasswordError}
                        onChangeText={this.onRepeatedPasswordChange}
                    ></AuthInput>
                    <FatButton text="Zarejestruj się" onPress={this.onSubmit}></FatButton>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    formContainer: {
        flex: 20,
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "center",
        zIndex: 1,
    },
    container: {
        flex: 1,
        justifyContent: "center",
        padding: 15
    },
    backButtonContainer: {
        flex: 1,
        alignItems: "flex-end",
        zIndex: 2,
        overflow:"visible"
    }
})
