import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'

interface Props {
    text: string;
    onPress: () => void,
}

export default class FatButton extends Component<Props> {
  render() {
    return (
      <TouchableOpacity style={styles.buttonStyles} onPress={this.props.onPress}>
        <Text style={styles.textStyle}> {this.props.text} </Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
    buttonStyles: {
        flex:1,
        borderColor: "black",
        borderStyle: "solid",
        borderWidth: 3,
        borderRadius: 40,
        padding: 5,
        margin: 10,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        maxHeight: 70,
        minHeight: 40
    },
    textStyle: {
        textAlign: "center",
        color: "black",
        fontSize: 20
    }
})
