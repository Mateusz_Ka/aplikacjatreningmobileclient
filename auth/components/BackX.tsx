import React, { Component } from 'react'
import { Text, TouchableOpacity, StyleSheet, Keyboard } from 'react-native'
import { NavigationProps } from '../types';

interface Props extends NavigationProps {

}

export default class BackX extends Component<Props> {
  render() {
    return (
      <TouchableOpacity onPress={this.onPress} style={styles.container}>
        <Text style={styles.textStyle}> X </Text>
      </TouchableOpacity>
    )
  }

    private onPress = () => {
        Keyboard.dismiss();
        this.props.navigation.navigate("Home")
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: "row",
    },
    textStyle: {
        textAlign: "center",
        color: "black",
        fontSize: 20,
    }
})
