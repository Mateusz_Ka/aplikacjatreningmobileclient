import React, { Component } from 'react'
import { Text, View, TextInput, StyleSheet } from 'react-native'

interface Props {
    inputType: TypeProp
    onEndEditing: () => void;
    onChangeText: (val: string) => void
    error?: string;
    value?: string;
}

interface TypeProp {
    placeholder: string;
    textContentType: "emailAddress" | "password";
    keyBoardType: "email-address" | "default";
    selectTextOnFocus: boolean;
    secureTextEntry: boolean;
}

export const emailInputProp: TypeProp = {
    placeholder: "email",
    textContentType: "emailAddress",
    keyBoardType: "email-address",
    selectTextOnFocus: true,
    secureTextEntry: false,
}

export const passwordInputProp: TypeProp = {
    placeholder: "hasło",
    textContentType: "password",
    keyBoardType: "default",
    selectTextOnFocus: false,
    secureTextEntry: true,
}

export class AuthInput extends Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    private onEndEditing = () => {
        this.props.onEndEditing()
    }
    private onChangeText = (text: string) => {
        this.props.onChangeText(text);
    }


    private renderError = () => {
        if (this.props.error) {
            return (<Text style={styles.errorText}>{this.props.error}</Text>)
        }
    }
    render() {
        const borderColor = this.props.error ? styles.errorColorStyle : styles.defaultColorStyle;
        return (
            <View style={styles.container}>
                <TextInput
                    style={{...styles.input, ...borderColor}}
                    placeholder={this.props.inputType.placeholder}
                    textContentType={this.props.inputType.textContentType}
                    keyboardType={this.props.inputType.keyBoardType}
                    selectTextOnFocus={this.props.inputType.selectTextOnFocus}
                    secureTextEntry={this.props.inputType.secureTextEntry}
                    onEndEditing={this.onEndEditing}
                    onFocus={() => this.setState({ text: "" })}
                    onChangeText={this.onChangeText}
                    value={this.props.value}
                    placeholderTextColor={this.props.error ? "red": "grey"}
                />
                {this.renderError()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    defaultColorStyle: {
        borderBottomColor: "black",
        color: "black"
    },
    errorColorStyle: {
        borderBottomColor: "red",
        color: "red"
    },
    input: {
        flex: 2,
        borderStyle: "solid",
        borderBottomWidth: 3,
        fontSize: 22,
        maxHeight: 50
    },
    container: {
        flex: 1,
        margin: 0,
        padding: 0,
        flexDirection: "column",
        justifyContent: "flex-start",
        maxHeight: 80,
        overflow: "visible"
    },
    errorText: {
        flex: 1,
        fontSize: 14,
        color: "red",
        overflow: "visible"
    }
})
