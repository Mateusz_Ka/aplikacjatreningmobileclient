import { NavigationScreenProp, NavigationContainer } from "react-navigation";

export interface NavigationProps {
    navigation: NavigationScreenProp<NavigationContainer>;
}
