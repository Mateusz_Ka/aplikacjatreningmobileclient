import React, { Component } from 'react'
import { Text, View, StyleSheet, ImageBackground, Image, TouchableOpacity } from 'react-native'

interface Props {
    onPress: () => void
}

export default class FloatingFatButton extends Component<Props> {
  render() {
    return (
        <TouchableOpacity style={style.container} onPress={this.props.onPress}>
            <Image source={require("../../assets/png/plus.png")} style={{width: '100%', height: '100%'}}/>
        </TouchableOpacity>
    )
  }
}

const style = StyleSheet.create({
    container: {
        borderRadius: 45,
        width: 70,
        height: 70,
        padding: 15,
        borderWidth: 2,
        borderColor: "black",
        borderStyle: "solid",
        position: "absolute",
        right: 30,
        top: 530,
        backgroundColor: "white",
        zIndex: 999,
    }
})
