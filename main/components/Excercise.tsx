import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { ExceriseData } from './ExcerciseInput';

interface Props {
    data: ExceriseData
}

export default class Excercise extends Component<Props> {
  render() {
    return this.props.data.oneSide ? (
      <View style={style.container}>
        <Text> Nazwa {this.props.data.name} </Text>
        <Text> Serie {this.props.data.series} </Text>
        <Text> Powtórzenia - Lewa strona {this.props.data.leftSideTImes} </Text>
        <Text> Powtórzenia - Prawa strona {this.props.data.rightSideTimes} </Text>
        <Text> Obciążenie - Lewa strona{this.props.data.leftSideWidth} </Text>
        <Text> Obciążenie - Prawa strona{this.props.data.rightSideWidth} </Text>
      </View>
    ) :
    (
        <View style={style.container}>
          <Text> Nazwa {this.props.data.name} </Text>
          <Text> Serie {this.props.data.series} </Text>
          <Text> Powtórzenia {this.props.data.times} </Text>
          <Text> Obciążenie {this.props.data.weight} </Text>
        </View>
      )
  }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        fontSize: 18,
        borderWidth: 1,
        borderColor: "grey",
        borderStyle: "solid",
        padding: 5,
    }
})
