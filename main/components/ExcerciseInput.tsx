import React, { Component } from 'react'
import { Text, View, Switch, StyleSheet } from 'react-native'
import Input from './Input';
import FatButton from '../../auth/components/FatButton';

interface Props {
    onSubmit: (data: ExceriseData) => void
}

export interface ExceriseData {
    oneSide: boolean;
    name?: string;
    series?: string;
    times?: string;
    leftSideTImes?: string;
    rightSideTimes?: string;
    weight?: string;
    leftSideWidth?: string;
    rightSideWidth?: string
}

export default class ExcerciseInput extends Component<Props, ExceriseData> {

    constructor(props: Props) {
        super(props);
        this.state = {
            oneSide: false,
        }
    }

    private renderInputs = () => {
        return this.state.oneSide ?
            (
                <View style={style.inputs}>
                    <Input placeholder="nazwa" onEndEditing={(value) => this.setState({ name: value })} keyboardType="default"></Input>
                    <Input placeholder="serie" onEndEditing={(value) => this.setState({ series: value })} keyboardType="numeric"></Input>
                    <Input placeholder="powtórzenia prawa strona" onEndEditing={(value) => this.setState({ rightSideTimes: value })} keyboardType="numeric"></Input>
                    <Input placeholder="powtórzenia lewa strona" onEndEditing={(value) => this.setState({ leftSideTImes: value })} keyboardType="numeric"></Input>
                    <Input placeholder="obciążenie prawa strona" onEndEditing={(value) => this.setState({ rightSideWidth: value })} keyboardType="numeric"></Input>
                    <Input placeholder="obciążenie lewa strona" onEndEditing={(value) => this.setState({ leftSideWidth: value })} keyboardType="numeric"></Input>
                    <FatButton text="zapisz" onPress={() => this.props.onSubmit(this.state)}></FatButton>
                </View>
            ) :
            (
                <View style={style.inputs}>
                    <Input placeholder="nazwa" onEndEditing={(value) => this.setState({ name: value})} keyboardType="default"></Input>
                    <Input placeholder="serie" onEndEditing={(value) => this.setState({ series: value})} keyboardType="numeric"></Input>
                    <Input placeholder="powtórzenia" onEndEditing={(value) => this.setState({ times: value})} keyboardType="numeric"></Input>
                    <Input placeholder="obciążenie" onEndEditing={(value) => this.setState({ weight: value})} keyboardType="numeric"></Input>
                    <FatButton text="zapisz" onPress={() => this.props.onSubmit(this.state)}></FatButton>
                </View>
            )
    }

    render() {
        return (
            <View style={style.container}>
                <View style={style.option}>
                    <Text style={{ textAlign: "center", flex: 1 }}>Ćwiczenie Jednoręczne</Text>
                    <Switch value={this.state.oneSide} onValueChange={(value: boolean) => this.setState({ oneSide: value })}></Switch>
                </View>
                {this.renderInputs()}
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "stretch"
    },
    option: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        maxHeight: 60,
        alignItems: "center"
    },
    inputs: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "flex-start",
    }
})
