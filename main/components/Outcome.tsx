import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { ExceriseData as TraningData } from './ExcerciseInput';

interface Props {
    traning: TraningData;
    key:number;
}

export default class Outcome extends Component<Props> {
    render() {
        return !this.props.traning.oneSide ? (
            <View style={style.container}>
                <Text style={style.name}> {this.props.traning.name} </Text>
                <View style={style.row}>
                    <Text>Serie: {this.props.traning.series}</Text>
                </View>
                <View style={style.row}>
                    <Text>Powtórzenia: {this.props.traning.times}</Text>
                </View>
                <View style={style.row}>
                    <Text>Obciążenie: {this.props.traning.weight}</Text>
                </View>
            </View>
        ) : (
                <View style={style.container}>
                    <Text style={style.name}> {this.props.traning.name} </Text>
                    <View style={style.row}>
                        <Text>Serie: {this.props.traning.series}</Text>
                    </View >
                    <View style={style.row}>
                        <Text>Powtórzenia Prawa Strona: {this.props.traning.rightSideTimes}</Text>
                    </View>
                    <View style={style.row}>
                        <Text>Powtórzenia Lewa Strona: {this.props.traning.leftSideTImes}</Text>
                    </View>
                    <View style={style.row}>
                        <Text>Obciążenie Lewa Strona: {this.props.traning.leftSideWidth}</Text>
                    </View>
                    <View style={style.row}>
                        <Text>Obciążenie Prawa Strona: {this.props.traning.rightSideWidth}</Text>
                    </View>
                </View>)
  }
}


const style = StyleSheet.create({
    container: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: "black",
        borderStyle: "solid",
        alignItems: "center",
    },
    name: {
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center",
        color: "black"
    },
    row: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    }
})