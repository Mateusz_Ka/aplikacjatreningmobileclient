import React, { Component } from 'react'
import { Text, View, TextInput, StyleSheet } from 'react-native'

interface Props {
    placeholder: string;
    onEndEditing: (text: string) => void;
    keyboardType: "default" | "numeric";
    onlyEndEditing?: (text: string) => void;
}

interface State {
    text: string;
}

export default class Input extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { text: "" }
    }

    private onEndEditing = () => {
        this.props.onEndEditing(this.state.text)
        if (this.props.onlyEndEditing) {
            this.props.onlyEndEditing(this.state.text);
        }
    }
    private onChangeText = (text: string) => {
        this.setState({ text })
        this.props.onEndEditing(text)
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    style={styles.input}
                    placeholder={this.props.placeholder}
                    keyboardType={this.props.keyboardType}
                    selectTextOnFocus={true}
                    onEndEditing={this.onEndEditing}
                    onChangeText={this.onChangeText}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 0,
        padding: 0,
        flexDirection: "column",
        justifyContent: "flex-start",
        maxHeight: 60,
        minHeight: 40
    },
    input: {
        flex: 1,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "black",
        fontSize: 18,
        maxHeight: 60,
        minHeight: 40
    },
})
