import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { ExceriseData } from './ExcerciseInput';
import Input from './Input';
import FatButton from '../../auth/components/FatButton';

interface Props {
    excercise: ExceriseData;
    onSubmit: (exercise: ExceriseData) => void;
}

interface State extends ExceriseData {
    exercise?: JSX.Element;
}

export default class Trening extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            name: this.props.excercise.name,
            oneSide: this.props.excercise.oneSide,
            exercise: !this.props.excercise.oneSide ? (
                <View style={style.container}>
                    <Text style={style.name}> {this.props.excercise.name} </Text>
                    <View style={style.row}>
                        <Text>Serie </Text>
                        <Input placeholder="zrobiono" keyboardType="numeric" onEndEditing={(text) => this.setState({ series: text })}></Input>
                    </View>
                    <View style={style.row}>
                        <Text>Powtórzenia </Text>
                        <Input placeholder="zrobiono" keyboardType="numeric" onEndEditing={(text) => this.setState({ times: text })}></Input>
                    </View>
                    <View style={style.row}>
                        <Text>Obciążenie </Text>
                        <Input placeholder="zrobiono" keyboardType="numeric" onEndEditing={(text) => this.setState({ weight: text })}></Input>
                    </View>
                    <FatButton text="Zakończ" onPress={() => {
                        this.setState({ exercise: (<View></View>) })
                        this.props.onSubmit(this.state);
                    }}></FatButton>
                </View>
            ) : (
                    <View style={style.container}>
                        <Text style={style.name}> {this.props.excercise.name} </Text>
                        <View style={style.row}>
                            <Text>Serie </Text>
                            <Input placeholder="zrobiono" keyboardType="numeric" onEndEditing={(text) => this.setState({ series: text })}></Input>
                        </View >
                        <View style={style.row}>
                            <Text>Powtórzenia Prawa Strona </Text>
                            <Input placeholder="zrobiono" keyboardType="numeric" onEndEditing={(text) => this.setState({ rightSideTimes: text })}></Input>
                        </View>
                        <View style={style.row}>
                            <Text>Powtórzenia Lewa Strona </Text>
                            <Input placeholder="zrobiono" keyboardType="numeric" onEndEditing={(text) => this.setState({ leftSideTImes: text })}></Input>
                        </View>
                        <View style={style.row}>
                            <Text>Obciążenie Lewa Strona</Text>
                            <Input placeholder="zrobiono" keyboardType="numeric" onEndEditing={(text) => this.setState({ leftSideWidth: text })}></Input>
                        </View>
                        <View style={style.row}>
                            <Text>Obciążenie Prawa Strona</Text>
                            <Input placeholder="zrobiono" keyboardType="numeric" onEndEditing={(text) => this.setState({ rightSideWidth: text })}></Input>
                        </View>
                        <FatButton text="Zakończ" onPress={() => {
                        this.setState({ exercise: (<View></View>) })
                        this.props.onSubmit(this.state);
                    }}></FatButton>
                    </View>
                )
        }
    }

    render() {
        return this.state.exercise
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        borderWidth: 2,
        borderColor: "black",
        borderStyle: "solid",
        alignItems: "center",
    },
    name: {
        fontSize: 30,
        fontWeight: "bold",
        textAlign: "center",
        color: "black"
    },
    row: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    }
})
