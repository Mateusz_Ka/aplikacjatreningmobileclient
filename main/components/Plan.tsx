import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import FatButton from '../../auth/components/FatButton';

export interface PlanProps {
    name: string;
    id: number;
    onPlanPress: (name: string, id: number) => void
    onTreningPress: (name: string, id: number) => void
    onFinishedTreningPress: (name: string, id: number) => void
}


export default class Plan extends Component<PlanProps> {
    private onPlanPress = () => this.props.onPlanPress(this.props.name, this.props.id);
    private onTreningPress = () => this.props.onTreningPress(this.props.name, this.props.id);
    private onFinishedTreningPress = () => this.props.onFinishedTreningPress(this.props.name, this.props.id);

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>{this.props.name}</Text>
                <View style={styles.buttons}>
                    <FatButton text="Ćwiczenia" onPress={this.onPlanPress}></FatButton>
                    <FatButton text="Treningi" onPress={this.onFinishedTreningPress}></FatButton>
                </View>
                    <FatButton text="Rozpocznij trening" onPress={this.onTreningPress}></FatButton>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderWidth: 2,
        borderColor: "black",
        borderStyle: "solid"
    },
    text: {
        fontSize: 30,
        textAlign: "center",
        fontWeight: "bold",
        color: "black",
        paddingTop: 15,
        paddingBottom: 15,
    },
    buttons: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    }
})
