import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet } from 'react-native'
import { NavigationProps } from '../../auth/types';
import { TreningData } from './TreningScreen';
import Outcome from '../components/Outcome';
import { BACKEND_URL } from '../../config';

interface Props extends NavigationProps {

}

interface State {
    trenings: TreningData[];
    planId?: number;
}

export default class DoneTreningScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            trenings: [],
            planId: this.props.navigation.state.params ? this.props.navigation.state.params.id : undefined,
        }
    }

    componentDidMount() {
        this.getTrenings();
    }

    getTrenings = () => fetch(`${BACKEND_URL}plan/${this.state.planId}/trening/all`)
        .then(res => res.json())
        .then(trenings => this.setState({ trenings }))

    render() {
        this.state.trenings.sort((a, b) => a.timestamp - b.timestamp);
        const map: Map<number, TreningData[]> = new Map();
        for (let i = 0; i < this.state.trenings.length; i++) {
            const trening = this.state.trenings[i];
            const mapArr = map.get(trening.timestamp);
            if (mapArr) {
                mapArr.push(trening);
            } else {
                map.set(trening.timestamp, [trening])
            }
        }
        const trenings = Array.from(map);
        console.log(trenings);
        return (
            <FlatList
                data={trenings}
                renderItem={(item) => <View style={style.container}>
                    <Text style={style.name}>Trening nr {item.index + 1} {this.props.navigation.state.params.name}</Text>
                    <View style={style.row}>
                        {item.item[1].map((t, index) => (<Outcome traning={t} key={index}></Outcome>))}
                    </View>
                </View>}
                keyExtractor={(item, index) => index.toString()}
            >
            </ FlatList>
        )
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        borderBottomWidth: 2,
        borderBottomColor: "black",
        borderStyle: "solid",
        alignItems: "center",
    },
    name: {
        fontSize: 30,
        fontWeight: "bold",
        textAlign: "center",
        color: "black"
    },
    row: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        flexWrap: "wrap",
        alignContent: "flex-start"
    }
})
