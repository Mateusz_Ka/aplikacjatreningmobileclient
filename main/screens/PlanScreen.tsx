import React, { Component } from 'react'
import { Text, View, StyleSheet, Modal } from 'react-native'
import FloatingFatButton from '../components/FloatingFatButton';
import ExcerciseInput, { ExceriseData } from '../components/ExcerciseInput';
import { NavigationProps } from '../../auth/types';
import { FlatList } from 'react-native-gesture-handler';
import Excercise from '../components/Excercise';
import { BACKEND_URL } from '../../config';

interface Props extends NavigationProps {

}

interface State {
    excercises: ExceriseData[];
    input?: JSX.Element;
    planId?: number;
}

export default class PlanScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            excercises: [],
            input: undefined,
            planId: this.props.navigation.state.params ? this.props.navigation.state.params.id : undefined,
        }
    }

    componentDidMount() {
        this.getExercises();
    }

    getExercises = () => fetch(`${BACKEND_URL}plan/${this.state.planId}/all`)
        .then(res => res.json())
        .then(excercises => this.setState({ excercises, input: undefined }))

    addExcerise = (excercise: ExceriseData) => {
        const obj = {
            method: "POST",
            body: JSON.stringify(excercise),
            headers: { 'Content-Type': 'application/json' },
        }
        fetch(`${BACKEND_URL}plan/${this.state.planId}/add`, obj)
            .then(() => this.getExercises())
            .catch(console.log)
    }

    render() {
        return (
            <View style={style.container}>
                {this.state.input}
                <FloatingFatButton
                    onPress={() => this.setState({ input: (<Modal><ExcerciseInput onSubmit={this.addExcerise}></ExcerciseInput></Modal>) })}
                ></FloatingFatButton>
                <FlatList
                    style={style.list}
                    data={this.state.excercises}
                    renderItem={(item) => <Excercise data={item.item}></Excercise>}
                    keyExtractor={(item, index) => index.toString()}></FlatList>
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "stretch"
    },
    list: {
        flex: 5,
    }
})
