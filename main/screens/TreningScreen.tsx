import React, { Component } from 'react'
import { Text, View, FlatList } from 'react-native'
import { NavigationProps } from '../../auth/types';
import { ExceriseData } from '../components/ExcerciseInput';
import Trening from '../components/Trening';
import FatButton from '../../auth/components/FatButton';
import { BACKEND_URL } from '../../config';

interface Props extends NavigationProps {

}

export interface TreningData extends ExceriseData {
    timestamp: number;
}

interface State {
    excercises: ExceriseData[];
    planId?: number;
    exercisesDoneCount: number;
    doneExercises: TreningData[];
    now: number;
}

export default class TreningScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            excercises: [],
            planId: this.props.navigation.state.params ? this.props.navigation.state.params.id : undefined,
            exercisesDoneCount: 0,
            doneExercises: [],
            now: Date.now(),
        }
    }

    componentDidMount() {
        this.getExercises();
    }

    getExercises = () => fetch(`${BACKEND_URL}plan/${this.state.planId}/all`)
        .then(res => res.json())
        .then(excercises => this.setState({ excercises }))

    handleSubmit = (exercise: any) => {
        delete exercise.exercise;
        exercise.timestamp = this.state.now;
        const newDoneCount = this.state.exercisesDoneCount;
        console.log(exercise);
        if (newDoneCount === this.state.excercises.length) {
            const obj = { 
                method: "POST", 
                body: JSON.stringify(exercise),
                headers: { 'Content-Type': 'application/json' },
             }
            fetch(`${BACKEND_URL}plan/${this.props.navigation.state.params.id}/trening/done`, obj)
                .then(() => this.props.navigation.navigate({
                    routeName: "Plan",
                    params: { name: this.props.navigation.state.params.name, id: this.props.navigation.state.params.id }
                }))
                .catch(console.log)
        } else {
            this.setState({exercisesDoneCount: this.state.exercisesDoneCount + 1})
            const obj = { 
                method: "POST", 
                body: JSON.stringify(exercise),
                headers: { 'Content-Type': 'application/json' },
             }
            fetch(`${BACKEND_URL}plan/${this.props.navigation.state.params.id}/trening/done`, obj)
                .catch(console.log)
        }
    }

    render() {
        return (
            <View>
                <FlatList 
                data={this.state.excercises}
                renderItem={(item) => <Trening excercise={item.item} onSubmit={this.handleSubmit}></Trening>}
                keyExtractor={(item, index) => index.toString()}></FlatList>
                
            </View>
        )
    }
}
