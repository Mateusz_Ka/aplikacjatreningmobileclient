import React, { Component } from 'react'
import { Text, View, FlatList, TextInput, StyleSheet } from 'react-native'
import FloatingFatButton from '../components/FloatingFatButton';
import Plan, { PlanProps } from '../components/Plan';
import Input from '../components/Input';
import { NavigationProps } from '../../auth/types';
import { BACKEND_URL } from '../../config';

interface Props extends NavigationProps {

}

interface State {
    plans: { id: number, name: string }[];
    input?: JSX.Element,
}

export default class PlansListScreen extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            plans: [],
            input: undefined,
        }
    }

    componentDidMount() {
        this.getPlans();
    }

    private getPlans = async () => {
        fetch(`${BACKEND_URL}plan/all`)
            .then(res => res.json())
            .then(plans => this.setState({ plans, input: undefined }))
    }

    private addPlan = async (name: string) => {
        const obj = { 
            method: "POST", 
            body: JSON.stringify({ name }),
            headers: { 'Content-Type': 'application/json' },
         }
        await fetch(`${BACKEND_URL}plan/add`, obj)
            .then(() => this.getPlans())
            .catch(console.log)
    }
    private showInput = () => {
        this.setState({
            input: (<Input
                placeholder="Nazwa projektu"
                onEndEditing={() => console.log()}
                keyboardType="default"
                onlyEndEditing={this.addPlan}
            ></Input>)
        })
    }

    private onPlanPress = (name: string, id: number) => this.props.navigation.navigate({
        routeName: "Plan",
        params: { name, id }
    })
    private onTreningPress = (name: string, id: number) => this.props.navigation.navigate({
        routeName: "Trening",
        params: { name, id }
    })
    private onFinishedTreningPress = (name: string, id: number) => this.props.navigation.navigate({
        routeName: "DoneTrening",
        params: { name, id }
    })

    render() {
        return (
            <View style={style.container}>
                {this.state.input}
                <FlatList
                    style={style.list}
                    data={this.state.plans}
                    renderItem={(item) => <Plan 
                        id={item.item.id} 
                        name={item.item.name}
                        onPlanPress={this.onPlanPress}
                        onTreningPress={this.onTreningPress}
                        onFinishedTreningPress={this.onFinishedTreningPress}
                        ></Plan>}
                    keyExtractor={(item, index) => index.toString()}
                ></FlatList>
                <FloatingFatButton onPress={this.showInput}></FloatingFatButton>
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "stretch"
    },
    list: {
        flex: 5,
    }
})
